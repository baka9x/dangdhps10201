package com.danghai.dangdhps10201.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.danghai.dangdhps10201.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText email, password;
    CheckBox checkSave;
    Button btnLogin, btnClear;
    TextView tvRegister, tvForgetPassword;
    FirebaseAuth auth;
    ProgressDialog loadingBar;
    final int MY_PERMISSIONS_REQUEST_INTERNET = 999;
    SharedPreferences data;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Đăng nhập tài khoản");
        //Ánh xạ
        init();
        requestPermissionInternet();
        //Xóa trắng form
        clearEditText();


        //Save form value
        saveInfo();

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        tvForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allowingUserToLogin();
            }
        });
    }


    //Check currentUser Login
    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }


    //Login Account with Firebase Auth
    private void allowingUserToLogin() {
        final String txt_email = email.getText().toString().trim();
        final String txt_password = password.getText().toString().trim();
        if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)){
            Toast.makeText(this, "Không được bỏ trống Fields", Toast.LENGTH_SHORT).show();
        }else{

            loadingBar.setTitle("Thông báo");
            loadingBar.setMessage("Chờ xíu, tài khoản của bạn đang được đăng nhập...");
            loadingBar.show();
            loadingBar.setCanceledOnTouchOutside(true);
            auth.signInWithEmailAndPassword(txt_email, txt_password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                sendUserToMainActivity();
                                Toast.makeText(LoginActivity.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                                //Save form login
                                SharedPreferences.Editor editor = data.edit();

                                if (checkSave.isChecked()) {
                                    //Luu tru thong tin vao file

                                    editor.putString("email", txt_email);
                                    editor.putString("password", txt_password);

                                }
                                editor.putBoolean("save_information", true);
                                editor.apply();
                            }else{

                                String msg = task.getException().getMessage();
                                Toast.makeText(LoginActivity.this, "LỖI: " + msg, Toast.LENGTH_SHORT).show();
                            }
                            loadingBar.dismiss();
                        }
                    });
        }
    }

    //Move to MainActivity
    private void sendUserToMainActivity(){
        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    private void clearEditText() {
        email.setText("");
        password.setText("");
        checkSave.setChecked(false);
    }

    private void init() {

        email = findViewById(R.id.edtEmail);
        password = findViewById(R.id.edtPassword);
        checkSave = findViewById(R.id.chkSave);
        btnLogin = findViewById(R.id.btnLogin);
        btnClear = findViewById(R.id.btnClear);
        tvRegister = findViewById(R.id.tvRegister);
        tvForgetPassword = findViewById(R.id.tvForgetPassword);
        loadingBar = new ProgressDialog(this);
        auth = FirebaseAuth.getInstance();

    }


    protected void requestPermissionInternet(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.INTERNET)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.INTERNET},
                        MY_PERMISSIONS_REQUEST_INTERNET);

            }
        } else {
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_INTERNET: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return;
            }
        }
    }


    public void saveInfo(){
        data = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        //Nạp thông tin lên form từ sharedPreference
        Boolean dataSave = data.getBoolean("save_information", false);
        if (dataSave) {
            email.setText(data.getString("email", ""));
            password.setText(data.getString("password", ""));
            checkSave.setChecked(true);
        }
    }
}
