package com.danghai.dangdhps10201.dialog;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.danghai.dangdhps10201.R;
import com.danghai.dangdhps10201.model.Book;
import static com.danghai.dangdhps10201.activity.MainActivity.count;
import static com.danghai.dangdhps10201.fragment.BookFragment.btnResetCart;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CartDialog extends DialogFragment {

    ImageView ivThumnail;
    TextView tvTitle, tvAuthor, tvCategory, tvPublisher, tvPrice;

    Button btnOk, btnCancel;

    List<Book> mList;


    public interface OnPassDataItemSelected {
        void sendData(String thumbnail, String title, String author, String category, String publisher, String price, int count);

    }

    public OnPassDataItemSelected mOnPassDataItemSelected;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_cart, container, false);


        //Ánh xạ
        ivThumnail = view.findViewById(R.id.iv_book_img);
        tvTitle = view.findViewById(R.id.tv_book_title);
        tvAuthor = view.findViewById(R.id.tv_book_author);
        tvCategory = view.findViewById(R.id.tv_book_category);
        tvPublisher = view.findViewById(R.id.tv_book_publisher);
        tvPrice = view.findViewById(R.id.tv_book_price);


        btnCancel = view.findViewById(R.id.btn_cancel);
        btnOk = view.findViewById(R.id.btn_ok);

        mList = new ArrayList<>();



        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = tvTitle.getText().toString();

                String author = tvAuthor.getText().toString();

                String publisher = tvPublisher.getText().toString();

                String price = tvPrice.getText().toString();

                String category = tvCategory.getText().toString();


                Bundle b = getArguments();
                String thumbnail = b.getString("ivThumbnail");;
                btnResetCart.setVisibility(View.VISIBLE);
                ++count;

                mOnPassDataItemSelected.sendData(thumbnail, title, author, category, publisher, price, count);
                getDialog().dismiss();
            }


        });


        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();

        String txtTitle = bundle.getString("tvTitle");
        String txtAuthor = bundle.getString("tvAuthor");
        String txtPublisher = bundle.getString("tvPublisher");
        String txtPrice = bundle.getString("tvPrice");
        String txtThumbnail = bundle.getString("ivThumbnail");
        String txtCategoryTitle = bundle.getString("tvCategoryTitle");
        tvTitle.setText(txtTitle);
        tvAuthor.setText(txtAuthor);
        tvPublisher.setText(txtPublisher);
        tvPrice.setText(txtPrice);
        tvCategory.setText(txtCategoryTitle);
        Picasso.get().load(txtThumbnail).into(ivThumnail);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mOnPassDataItemSelected = (OnPassDataItemSelected) getTargetFragment();
        } catch (ClassCastException ex) {
            Log.e("Error", "ClassCastException" + ex.getMessage());
        }
    }
}
