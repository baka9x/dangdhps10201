package com.danghai.dangdhps10201.dao;

import android.content.Context;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.adapter.CategoryAdapter;
import com.danghai.dangdhps10201.adapter.SpinnerAdapter;
import com.danghai.dangdhps10201.adapter.ViewPagerAdapter;
import com.danghai.dangdhps10201.fragment.BookFragment;
import com.danghai.dangdhps10201.model.Category;
import com.danghai.dangdhps10201.model.NonUI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CategoryDAO {

    NonUI nonUI;
    private DatabaseReference reference;
    Context context;
    List<Category> mCategory;
    String catId;

    public CategoryDAO(Context context) {

        this.reference = FirebaseDatabase.getInstance().getReference("Categories");
        this.context = context;
        this.nonUI = new NonUI(context);

    }

    public void insert(Category category) {
        catId = reference.push().getKey();
        category.setId(catId);
        reference.child(category.getId()).setValue(category).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                nonUI.toast("Thêm thành công");

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                nonUI.toast("Thêm thất bại");

            }
        });


    }

    //Read all book to recyclerview
    public List<Category> readAllCategoryToRecyclerView(final RecyclerView recyclerView) {
        mCategory = new ArrayList<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Get Category object and use the values to update the UI
                mCategory.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Category category = data.getValue(Category.class);
                    mCategory.add(category);
                }
                CategoryAdapter categoryAdapter = new CategoryAdapter(context, mCategory);

                // Apply the adapter to the spinner
                recyclerView.setAdapter(categoryAdapter);
                categoryAdapter.notifyDataSetChanged();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        };
        reference.addValueEventListener(listener);


        return mCategory;
    }


    public List<Category> readAllCategoryToSpinner(final Spinner spinner) {
        mCategory = new ArrayList<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Get Category object and use the values to update the UI
                mCategory.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Category category = data.getValue(Category.class);
                    mCategory.add(category);
                }
                SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context, mCategory);

                // Apply the adapter to the spinner
                spinner.setAdapter(spinnerAdapter);
                spinnerAdapter.notifyDataSetChanged();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        };
        reference.addValueEventListener(listener);


        return mCategory;
    }

    //Retrieve all title Category
    public List<Category> readAllCategoryToBookFragment(final ViewPagerAdapter pagerAdapter) {
        mCategory = new ArrayList<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Get Category object and use the values to update the UI
                mCategory.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Category category = data.getValue(Category.class);
                    mCategory.add(category);
                }

                for (int i = 0; i < mCategory.size(); i++) {
                    pagerAdapter.addFragment(new BookFragment(mCategory.get(i).getId()), mCategory.get(i).getTitle());
                    pagerAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        };
        reference.addValueEventListener(listener);

        return mCategory;
    }




}
