package com.danghai.dangdhps10201.dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.danghai.dangdhps10201.activity.LoginActivity;
import com.danghai.dangdhps10201.model.NonUI;
import com.danghai.dangdhps10201.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;
import java.util.Objects;

public class UserDAO {

    NonUI nonUI;
    private FirebaseAuth auth;
    private DatabaseReference reference;
    Context context;

    String userId;
    List<User> mUser;


    public UserDAO(Context context) {
        this.auth = FirebaseAuth.getInstance();
        this.reference = FirebaseDatabase.getInstance().getReference("Users");
        this.context = context;
        this.nonUI = new NonUI(context);

    }



    public void insert(final User user, String email, String password, final ProgressDialog loadingBar) {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            userId = firebaseUser.getUid();
                            user.setId(userId);

                            reference.child(user.getId()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Intent intent = new Intent(context, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(intent);
                                    } else {
                                        String msg = Objects.requireNonNull(task.getException()).getMessage();
                                        Toast.makeText(context, "LỖI: " + msg, Toast.LENGTH_SHORT).show();
                                    }
                                    loadingBar.dismiss();

                                }
                            });

                        }else{
                            nonUI.toast("Bạn không thể đăng ký với email này!");
                            loadingBar.dismiss();
                        }
                    }
                });



    }
}
