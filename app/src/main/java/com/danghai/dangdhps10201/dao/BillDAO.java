package com.danghai.dangdhps10201.dao;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.adapter.BillAdapter;
import com.danghai.dangdhps10201.fragment.BillFragment;
import com.danghai.dangdhps10201.fragment.CartFragment;
import com.danghai.dangdhps10201.model.Bill;
import com.danghai.dangdhps10201.model.BillDetail;
import com.danghai.dangdhps10201.model.Cart;
import com.danghai.dangdhps10201.model.NonUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BillDAO {

    NonUI nonUI;
    private DatabaseReference reference;
    Context context;
    String billId;
    String detailId;

    List<Bill> mBill;
    List<BillDetail> mDetail;
    CartFragment fr;
    BillFragment fr2;


    public BillDAO(Context context) {
        this.reference = FirebaseDatabase.getInstance().getReference("Bill");
        this.context = context;
        this.nonUI = new NonUI(context);

    }

    public BillDAO(Context context, CartFragment fr) {

        this.reference = FirebaseDatabase.getInstance().getReference("Bill");
        this.context = context;
        this.nonUI = new NonUI(context);
        this.fr = fr;

    }

    public BillDAO(Context context, BillFragment fr2) {

        this.reference = FirebaseDatabase.getInstance().getReference("Bill");
        this.context = context;
        this.nonUI = new NonUI(context);
        this.fr2 = fr2;

    }

    //Read all book to recyclerview
    public List<Bill> readAllBillToRecyclerView() {
        mBill = new ArrayList<>();
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Get Category object and use the values to update the UI
                mBill.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Bill bill = data.getValue(Bill.class);
                    mBill.add(bill);
                }

                //Update listview from Bill Fragment
                fr2.updateRecyclerView();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        };
        reference.addValueEventListener(listener);


        return mBill;
    }



    public void insert(final Bill bill, final List<BillDetail> details, final BillDetail detail) {
        mDetail = new ArrayList<>();
        billId = reference.push().getKey();
        bill.setId(billId);
        reference.child(bill.getId()).setValue(bill).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference("DetailBill");
                detailId = ref.push().getKey();
                for (int i = 0; i < details.size(); i++){

                    detail.setId(detailId);
                    detail.setBillId(billId);
                    detail.setBookQuantity(details.get(i).getBookQuantity());
                    detail.setBookTitle(details.get(i).getBookTitle());
                    detail.setBookPrice(details.get(i).getBookPrice());
                    Log.d("DetailsDAO", details.size() + "");
                    Log.d("DetailsDAO", details.get(i).getBookTitle() + "");
                    mDetail.add(new BillDetail(detailId, details.get(i).getBookTitle(), details.get(i).getBookPrice(), details.get(i).getBookQuantity(), billId));

                    ref.child(detail.getId()).setValue(mDetail).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            nonUI.toast("Tạo hóa đơn thành công");
                            details.clear();


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            nonUI.toast("Có lỗi khi thêm sách vào hóa đơn");
                        }
                    });
                }



            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                nonUI.toast("Thêm thất bại");

            }
        });


    }


    public void delete(final Bill item) {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("id").getValue(String.class).equalsIgnoreCase(item.getId())) {
                        billId = data.getKey();

                        Log.d("getKey", "onCreate: key :" + billId);

                        reference.child(billId).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        nonUI.toast("Xóa item thành công");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("Xóa item thất bại");
                                        Log.d("delete", "delete That bai");
                                    }
                                });

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
