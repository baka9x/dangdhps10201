package com.danghai.dangdhps10201.dao;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.danghai.dangdhps10201.fragment.BookFragment;
import com.danghai.dangdhps10201.model.Book;
import com.danghai.dangdhps10201.model.Category;
import com.danghai.dangdhps10201.model.NonUI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BookDAO {

    NonUI nonUI;
    private DatabaseReference reference;
    Context context;
    String bookId;
    List<Book> mBook;

    BookFragment fr;


    public BookDAO(Context context) {
        this.reference = FirebaseDatabase.getInstance().getReference("Books");
        this.context = context;
        this.nonUI = new NonUI(context);
    }

    public BookDAO(Context context, BookFragment fr) {

        this.reference = FirebaseDatabase.getInstance().getReference("Books");
        this.context = context;
        this.nonUI = new NonUI(context);
        this.fr = fr;

    }

    //Read all book to recyclerview order by category on Spinner
    public List<Book> readAllBookOrderByCategory(final String categoryCode) {
        mBook = new ArrayList<>();

        Query query = reference.orderByChild("catId").startAt(categoryCode).endAt(categoryCode + "\uf8ff");

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Get Book object order by Category on Spinner and use the values to update the UI
                mBook.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Book book = data.getValue(Book.class);
                        mBook.add(book);

                }

                fr.updateRecyclerView();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        };
        query.addValueEventListener(listener);


        return mBook;
    }



    public void insert(Book book) {
        bookId = reference.push().getKey();
        book.setId(bookId);
        reference.child(book.getId()).setValue(book).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                nonUI.toast("Thêm thành công");

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                nonUI.toast("Thêm thất bại");

            }
        });


    }

    public void delete(final Book item) {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("id").getValue(String.class).equalsIgnoreCase(item.getId())) {
                        bookId = data.getKey();

                        Log.d("getKey", "onCreate: key :" + bookId);

                        reference.child(bookId).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        nonUI.toast("Xóa item thành công");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("Xóa item thất bại");
                                        Log.d("delete", "delete That bai");
                                    }
                                });

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public void update(final Book item) {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("maSach").getValue(String.class).equals(item.getId())){

                        bookId = data.getKey();




                        reference.child(bookId).setValue(item)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        nonUI.toast("update Thanh cong");
                                        Log.d("update","update Thanh cong");


                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("update That bai");
                                        Log.d("update","update That bai");
                                    }
                                });

                    }

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
