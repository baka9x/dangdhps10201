package com.danghai.dangdhps10201.dao;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.danghai.dangdhps10201.fragment.CartFragment;
import com.danghai.dangdhps10201.model.Bill;
import com.danghai.dangdhps10201.model.BillDetail;
import com.danghai.dangdhps10201.model.Cart;
import com.danghai.dangdhps10201.model.NonUI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CartDAO {

    NonUI nonUI;
    private DatabaseReference reference;
    Context context;
    String cardId;

    List<Cart> mCart;
    CartFragment fr;

    public CartDAO(Context context) {
        this.reference = FirebaseDatabase.getInstance().getReference("CartList");
        this.context = context;
        this.nonUI = new NonUI(context);

    }

    public CartDAO(Context context, CartFragment fr) {

        this.reference = FirebaseDatabase.getInstance().getReference("CartList");
        this.context = context;
        this.nonUI = new NonUI(context);
        this.fr = fr;

    }

    public void insert(Cart cart) {
        cardId = reference.push().getKey();
        cart.setId(cardId);
        reference.child(cart.getId()).setValue(cart).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                nonUI.toast("Thêm thành công");

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                nonUI.toast("Thêm thất bại");

            }
        });


    }


    //Read all cart of currentUser to recyclerview
    public List<Cart> readAllCartByUserId(final String userId) {

        mCart = new ArrayList<>();
        Query query = reference.orderByChild("userId").startAt(userId).endAt(userId + "\uf8ff");
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mCart.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Cart cart = data.getValue(Cart.class);
                    mCart.add(cart);
                }
                fr.updateRecyclerView();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        };
        query.addValueEventListener(listener);


        return mCart;
    }

    public void isDuplicateItem(final String title, final Cart item) {

        Query query = reference.orderByChild("title").startAt(title).endAt(title + "\uf8ff");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    nonUI.toast("To do this");
                } else {
                    nonUI.toast("Đã có item trong giỏ");
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    public void delete(final Cart item) {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot data : dataSnapshot.getChildren()) {

                    if (data.child("id").getValue(String.class).equalsIgnoreCase(item.getId())) {
                        cardId = data.getKey();

                        Log.d("getKey", "onCreate: key :" + cardId);

                        reference.child(cardId).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        nonUI.toast("Xóa item thành công");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        nonUI.toast("Xóa item thất bại");
                                        Log.d("delete", "delete That bai");
                                    }
                                });

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

}
