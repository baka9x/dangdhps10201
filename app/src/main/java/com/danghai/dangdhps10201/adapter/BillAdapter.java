package com.danghai.dangdhps10201.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.R;
import com.danghai.dangdhps10201.model.Bill;

import java.util.List;

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.BillHolder> {

    private Context mContext;
    private List<Bill> mList;

    private OnItemClickListener mListener;


    public interface OnItemClickListener {
        void onOptionItem(int position);


    }
    public void setItemClickListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }

    public BillAdapter(Context mContext, List<Bill> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public BillAdapter.BillHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bill_list, parent, false);

        return new BillAdapter.BillHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull BillAdapter.BillHolder holder, int position) {

        Bill bill = mList.get(position);

        holder.title.setText(bill.getId());

        holder.date.setText(bill.getDate());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class BillHolder extends RecyclerView.ViewHolder {
        TextView title, date;
        ImageView optionItem;


        public BillHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            title = itemView.findViewById(R.id.tv_bill_title);
            date = itemView.findViewById(R.id.tv_bill_date);
            optionItem = itemView.findViewById(R.id.iv_option_item);

            optionItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onOptionItem(position);
                        }
                    }
                }
            });
        }
    }
}
