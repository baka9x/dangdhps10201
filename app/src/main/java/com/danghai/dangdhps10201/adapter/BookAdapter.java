package com.danghai.dangdhps10201.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.R;
import com.danghai.dangdhps10201.model.Book;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookHolder> {

    private Context mContext;
    private List<Book> mList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
        void onOptionItemClick(int position);

    }

    public void setItemClickListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }


    public BookAdapter(Context mContext, List<Book> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }


    @NonNull
    @Override
    public BookHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false);

        return new BookAdapter.BookHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final BookHolder holder, int position) {
        final Book book = mList.get(position);

        holder.bookTitle.setText(book.getTitle());
        Picasso.get().load(book.getThumbUrl()).error(R.drawable.book).into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class BookHolder extends RecyclerView.ViewHolder {
        TextView bookTitle;
        ImageView thumbnail, option_img;

        public BookHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            bookTitle = itemView.findViewById(R.id.tv_book_title);
            thumbnail = itemView.findViewById(R.id.iv_book_img);
            option_img = itemView.findViewById(R.id.iv_option_item);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });

            option_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onOptionItemClick(position);
                        }
                    }
                }
            });


        }
    }


}
