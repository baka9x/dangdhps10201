package com.danghai.dangdhps10201.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.R;
import com.danghai.dangdhps10201.model.Cart;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder> {

    private Context mContext;
    private List<Cart> mList;
    private OnItemClickListener mListener;


    public interface OnItemClickListener {
        void onRemove(int position);

        void onDecrement(int position, int number);

        void onIncrement(int position, int number);

    }

    public void setItemClickListener(OnItemClickListener mListener) {
        this.mListener = mListener;
    }

    public CartAdapter(Context mContext, List<Cart> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public CartAdapter.CartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_list, parent, false);

        return new CartAdapter.CartHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapter.CartHolder holder, int position) {

        Cart cart = mList.get(position);
        holder.tvTitle.setText(cart.getTitle());
        holder.tvAuthor.setText(cart.getAuthor());
        holder.tvCategory.setText(cart.getCategory());
        holder.tvPulisher.setText(cart.getPublisher());
        holder.tvPrice.setText(cart.getPrice());

        Picasso.get().load(cart.getThumbnail()).into(holder.ivThumbnail);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class CartHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvCategory, tvAuthor, tvPulisher, tvPrice, tvNumber;
        Button btnRemove, btnDecrement, btnIncrement;
        ImageView ivThumbnail;
        int number = 1;

        public CartHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);



            tvTitle = itemView.findViewById(R.id.tv_book_title);
            tvCategory = itemView.findViewById(R.id.tv_book_category);
            tvPulisher = itemView.findViewById(R.id.tv_book_publisher);
            tvPrice = itemView.findViewById(R.id.tv_book_price);
            tvAuthor = itemView.findViewById(R.id.tv_book_author);

            ivThumbnail = itemView.findViewById(R.id.iv_book_img);
            btnRemove = itemView.findViewById(R.id.btn_remove_cart);
            btnDecrement = itemView.findViewById(R.id.btn_decrement);
            btnIncrement = itemView.findViewById(R.id.btn_increment);
            tvNumber = itemView.findViewById(R.id.tv_number);
            tvNumber.setText(String.valueOf(number));

            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onRemove(position);
                        }
                    }

                }
            });

            btnDecrement.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();


                        if (position != RecyclerView.NO_POSITION) {
                            if (number == 1) {
                                Toast.makeText(mContext, "Số lượng phải lớn hơn 1", Toast.LENGTH_SHORT).show();
                            } else {
                                number--;
                                listener.onDecrement(position, number);
                                tvNumber.setText(String.valueOf(number));
                            }
                        }
                    }
                }
            });

            btnIncrement.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {

                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            ++number;
                            listener.onIncrement(position, number);
                            tvNumber.setText(String.valueOf(number));
                        }
                    }
                }
            });

        }
    }
}
