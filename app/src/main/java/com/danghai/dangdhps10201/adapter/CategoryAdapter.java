package com.danghai.dangdhps10201.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.model.Category;
import com.danghai.dangdhps10201.R;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {


    private Context mContext;
    private List<Category> mlist;

    public CategoryAdapter(Context mContext, List<Category> mlist) {
        this.mContext = mContext;
        this.mlist = mlist;
    }


    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categories, parent, false);
        return new CategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        Category category = mlist.get(position);

        holder.tvGenreName.setText("Tên thể loại: " + category.getTitle());
        holder.tvDescription.setText("Mô tả: " + category.getDescription());
        holder.tvPosition.setText("Vị trí: " + category.getPosition());


    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvGenreCode, tvGenreName, tvDescription, tvPosition;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvGenreName = itemView.findViewById(R.id.tv_genre_name);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvPosition = itemView.findViewById(R.id.tv_position);
        }
    }
}
