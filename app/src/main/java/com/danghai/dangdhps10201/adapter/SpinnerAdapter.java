package com.danghai.dangdhps10201.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.danghai.dangdhps10201.model.Category;
import com.danghai.dangdhps10201.R;

import java.util.List;

public class SpinnerAdapter extends BaseAdapter {
    Context context;
    List<Category> mList;

    public SpinnerAdapter(Context context, List<Category> mList) {
        this.context = context;
        this.mList = mList;
    }

    public SpinnerAdapter() {
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_spinner_category, null);
        }

        //Ánh xạ
        TextView title = convertView.findViewById(R.id.sp_title);
        //Put data
        Category category = mList.get(position);

        title.setText(category.getTitle());

        return convertView;
    }
}
