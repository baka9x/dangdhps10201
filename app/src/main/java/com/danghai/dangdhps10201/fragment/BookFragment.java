package com.danghai.dangdhps10201.fragment;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.R;
import com.danghai.dangdhps10201.adapter.BookAdapter;
import com.danghai.dangdhps10201.dao.BookDAO;
import com.danghai.dangdhps10201.dao.CartDAO;
import com.danghai.dangdhps10201.dao.CategoryDAO;
import com.danghai.dangdhps10201.dialog.CartDialog;
import com.danghai.dangdhps10201.model.Book;
import com.danghai.dangdhps10201.model.Cart;
import com.danghai.dangdhps10201.model.Category;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.danghai.dangdhps10201.activity.MainActivity.count;

public class BookFragment extends Fragment implements CartDialog.OnPassDataItemSelected {
    public static Button btnResetCart;
    RecyclerView rvBook;
    List<Book> mBook;
    List<Category> mCategory;
    List<Cart> mCart;

    BookDAO bookDAO;
    CartDAO cartDAO;
    CategoryDAO categoryDAO;
    String categoryId;

    BookAdapter bookAdapter;
    MenuItem mCartShoppingItem;
    TextView mTvCartCount;
    ImageButton mCartImageBtn;

    //Upload image
    StorageReference storageReference;
    public final int IMAGE_REQUEST = 1;
    private Uri imageUri;
    private StorageTask uploadTask;
    DatabaseReference reference;

    public BookFragment() {

    }

    public BookFragment(String categoryId) {
        this.categoryId = categoryId;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_book, container, false);
        //Init
        init(view);
        //To do action when clicked item to cart
        buildAction();

        return view;
    }

    private void init(View view) {
        rvBook = view.findViewById(R.id.rv_book_list);
        btnResetCart = view.findViewById(R.id.btn_reset_cart);
        //btnResetCart.setVisibility(View.GONE);
        rvBook.setHasFixedSize(true);
        mCategory = new ArrayList<>();
        mBook = new ArrayList<>();
        mCart = new ArrayList<>();
        categoryDAO = new CategoryDAO(getContext());
        bookDAO = new BookDAO(getContext(), this);
        mBook = bookDAO.readAllBookOrderByCategory(categoryId);
        storageReference = FirebaseStorage.getInstance().getReference("Uploads");
        //Gridlayout with 3 cols
        rvBook.setLayoutManager(new GridLayoutManager(getContext(), 3));
    }

    private void buildAction() {
        bookAdapter = new BookAdapter(getContext(), mBook);
        rvBook.setAdapter(bookAdapter);

        bookAdapter.setItemClickListener(new BookAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                doActionCartDialog(position);

            }

            @Override
            public void onOptionItemClick(final int position) {
                PopupMenu popupMenu = new PopupMenu(getContext(), rvBook.getChildAt(position));//Show popup menu below id item layout
                popupMenu.inflate(R.menu.menu_book_item);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.add_thumbnail:
                                openImage(position);
                                break;
                            case R.id.edit_book:
                                Toast.makeText(getContext(), "Sửa", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.remove_book:
                                bookDAO.delete(mBook.get(position));
                                bookAdapter.notifyDataSetChanged();
                                break;
                            default:
                                break;
                        }


                        return false;
                    }
                });
                popupMenu.show();
            }
        });
        btnResetCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count = 0;
                refreshFragment();

            }
        });


    }

    //Show dialog Add new Cart

    private void doActionCartDialog(int position) {
        btnResetCart.setVisibility(View.GONE);
        Book book = mBook.get(position);
        //Fragment cartFragment = new CartFragment();
        Bundle bundle = new Bundle();
        bundle.putString("tvTitle", book.getTitle());
        bundle.putString("tvAuthor", book.getAuthor());
        bundle.putString("tvPublisher", book.getPublisher());
        bundle.putString("tvPrice", book.getPrice());
        bundle.putString("ivThumbnail", book.getThumbUrl());
        bundle.putString("tvCategoryTitle", book.getCatTitle());

        CartDialog cartDialog = new CartDialog();

        cartDialog.setTargetFragment(BookFragment.this, 1);
        cartDialog.setArguments(bundle);
        cartDialog.show(getFragmentManager(), "Thêm vào giỏ");

    }


    //count cart number menu textview
    private void doIncrement() {
        ++count;
        mTvCartCount.setText(String.valueOf(count));
    }


    //Refresh Fragment when clicked btnResetCart
    private void refreshFragment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).commitNow();
            getFragmentManager().beginTransaction().attach(this).commitNow();

        } else {
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_cart, menu);

        mCartShoppingItem = menu.findItem(R.id.cart_book);
        View actionView = mCartShoppingItem.getActionView();

        if (actionView != null) {
            mTvCartCount = actionView.findViewById(R.id.tv_cart_count);
            mCartImageBtn = actionView.findViewById(R.id.ib_cart_icon);
            mTvCartCount.setText(String.valueOf(count));
            mCartImageBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String txtCount = mTvCartCount.getText().toString().trim();
                    if (txtCount.equals("0")) {
                        Toast.makeText(getContext(), "Không có item nào được thêm vào giỏ!", Toast.LENGTH_SHORT).show();
                    } else {
                        //Go to CartFragment to do action
                        Fragment fragment = new CartFragment();
                        replaceFragment(fragment);


                    }
                }
            });

        }

    }


    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    //Choose Image and Upload Image to Firebase
    private void openImage(int position) {
        Intent intent = new Intent();
        intent.setType("image/*");

        //Put position to upload image function
        intent.putExtra("Position", position);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //Important: setIntent to getIntent on ActivityResult
        getActivity().setIntent(intent);
        startActivityForResult(intent, IMAGE_REQUEST);
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContext().getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uploadImage(final int position) {
        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setMessage("Uploading...");
        pd.show();

        if (imageUri != null) {
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis()
                    + "." + getFileExtension(imageUri));

            uploadTask = fileReference.putFile(imageUri);
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        String mUri = downloadUri.toString();
                        reference = FirebaseDatabase.getInstance().getReference("Books").child(mBook.get(position).getId());
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("thumbUrl", mUri);
                        reference.updateChildren(map);
                        pd.dismiss();

                    } else {
                        Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                        pd.dismiss();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            });
        } else {
            Toast.makeText(getContext(), "Chưa chọn ảnh", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {

            imageUri = data.getData();

            if (uploadTask != null && uploadTask.isInProgress()) {
                Toast.makeText(getContext(), "Đang upload...", Toast.LENGTH_SHORT).show();
            } else {
                //Get intent with getActivity().getIntent()
                int pos = getActivity().getIntent().getIntExtra("Position", 0);
                uploadImage(pos);

            }

        }
    }

    //Update recyclerView in Adapter
    public void updateRecyclerView() {

        bookAdapter.notifyItemInserted(mBook.size());

        bookAdapter.notifyDataSetChanged();


    }

    @Override
    public void sendData(String thumbnail, String title, String author, String category, String publisher, String price, int count) {

        mTvCartCount.setText(String.valueOf(count));

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        Cart cart = new Cart();
        cart.setTitle(title);
        cart.setAuthor(author);
        cart.setCategory(category);
        cart.setPublisher(publisher);
        cart.setPrice(price);
        cart.setThumbnail(thumbnail);
        cart.setUserId(firebaseUser.getUid());

        cartDAO = new CartDAO(getContext());
        cartDAO.insert(cart);

//            if (cartDAO.isDuplicateItem(title)) {
//
//            }



    }
}
