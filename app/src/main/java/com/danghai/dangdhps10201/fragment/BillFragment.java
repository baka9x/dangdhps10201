package com.danghai.dangdhps10201.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.R;
import com.danghai.dangdhps10201.adapter.BillAdapter;
import com.danghai.dangdhps10201.dao.BillDAO;
import com.danghai.dangdhps10201.model.Bill;

import java.util.ArrayList;
import java.util.List;

public class BillFragment extends Fragment {

    RecyclerView listView;

    List<Bill> mBill;

    BillDAO billDAO;

    BillAdapter billAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bill, container, false);

        listView = view.findViewById(R.id.list_view_bill);


        listView.setHasFixedSize(true);

        listView.setLayoutManager(new LinearLayoutManager(getContext()));

        mBill = new ArrayList<>();


        billDAO = new BillDAO(getContext(), this);


        mBill = billDAO.readAllBillToRecyclerView();

        billAdapter = new BillAdapter(getContext(), mBill);

        listView.setAdapter(billAdapter);


        billAdapter.setItemClickListener(new BillAdapter.OnItemClickListener() {
            @Override
            public void onOptionItem(final int position) {
                PopupMenu popupMenu = new PopupMenu(getContext(), listView.getChildAt(position));//Show popup menu below id item layout
                popupMenu.inflate(R.menu.menu_bill_item);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_bill_edit:
                                Toast.makeText(getContext(), "Sửa", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.menu_bill_remove:
                               billDAO.delete(mBill.get(position));
                                billAdapter.notifyDataSetChanged();
                                break;
                            default:
                                break;
                        }


                        return false;
                    }
                });
                popupMenu.show();
            }

        });


        return view;
    }


    //Update recyclerView in Adapter
    public void updateRecyclerView() {

        billAdapter.notifyItemInserted(mBill.size());

        billAdapter.notifyDataSetChanged();


    }
}
