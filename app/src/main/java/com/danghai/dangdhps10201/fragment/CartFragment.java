package com.danghai.dangdhps10201.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.R;
import com.danghai.dangdhps10201.adapter.CartAdapter;
import com.danghai.dangdhps10201.dao.BillDAO;
import com.danghai.dangdhps10201.dao.CartDAO;
import com.danghai.dangdhps10201.model.Bill;
import com.danghai.dangdhps10201.model.BillDetail;
import com.danghai.dangdhps10201.model.Cart;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class CartFragment extends Fragment {

    RecyclerView rvCart;
    Button btnCheckOut;
    List<Cart> mCart;
    List<Bill> mBill;
    List<BillDetail> mDetail;
    BillDAO billDAO;
    CartAdapter cartAdapter;
    FirebaseUser fUser;

    CartDAO cartDAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        rvCart = view.findViewById(R.id.rv_cart_list);
        btnCheckOut = view.findViewById(R.id.btn_check_out);

        rvCart.setHasFixedSize(true);
        mCart = new ArrayList<>();
        mBill = new ArrayList<>();
        mDetail = new ArrayList<>();

        cartDAO = new CartDAO(getContext(), this);
        billDAO = new BillDAO(getContext(), this);

        rvCart.setLayoutManager(new LinearLayoutManager(getContext()));

        fUser = FirebaseAuth.getInstance().getCurrentUser();

        mCart = cartDAO.readAllCartByUserId(fUser.getUid());


        cartAdapter = new CartAdapter(getContext(), mCart);

        rvCart.setAdapter(cartAdapter);
        final Bill bill = new Bill();
        final BillDetail detail = new BillDetail();
        final Cart cart = new Cart();
        cart.setQuantity(String.valueOf(1));

        detail.setBookQuantity(String.valueOf(1));

        cartAdapter.setItemClickListener(new CartAdapter.OnItemClickListener() {
            @Override
            public void onRemove(int position) {
                cartDAO.delete(mCart.get(position));
            }

            @Override
            public void onDecrement(int position, int number) {
                Cart cart = mCart.get(position);

                cart.setQuantity(String.valueOf(number));


            }

            @Override
            public void onIncrement(int position, int number) {
                Cart cart = mCart.get(position);

                cart.setQuantity(String.valueOf(number));

            }
        });

        btnCheckOut.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
                String currentDateandTime = sdf.format(new Date());
                bill.setDate(currentDateandTime);
                billDAO = new BillDAO(getContext());



                for (int i = 0; i < mCart.size(); i++) {

                    detail.setBookPrice(mCart.get(i).getPrice());
                    detail.setBookTitle(mCart.get(i).getTitle());
                    Log.i("mCart.get(i).getPrice()", mCart.get(i).getTitle());
                    mDetail.add(new BillDetail(mCart.get(i).getTitle(), mCart.get(i).getPrice(), mCart.get(i).getQuantity()));

                }


                Log.d("Detail", mDetail.size() + "");


                billDAO.insert(bill, mDetail, detail);


            }
        });


        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    //Update recyclerView in Adapter
    public void updateRecyclerView() {

        cartAdapter.notifyItemInserted(mCart.size());

        cartAdapter.notifyDataSetChanged();


    }
}
