package com.danghai.dangdhps10201.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.danghai.dangdhps10201.R;
import com.danghai.dangdhps10201.dao.UserDAO;
import com.danghai.dangdhps10201.model.User;

public class ProfileFragment extends Fragment {

    UserDAO userDAO;

    EditText username;
    EditText phoneNumber;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        //Anh xa
        init(view);

        return view;
    }

    private void init(View view) {

        username = view.findViewById(R.id.edt_username_profile);
        phoneNumber = view.findViewById(R.id.edt_phone_number_profile);

        userDAO = new UserDAO(getContext());
    }


}
