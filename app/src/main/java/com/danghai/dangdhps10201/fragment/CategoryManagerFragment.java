package com.danghai.dangdhps10201.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.dangdhps10201.dao.CategoryDAO;
import com.danghai.dangdhps10201.model.Category;
import com.danghai.dangdhps10201.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;


public class CategoryManagerFragment extends Fragment {

    EditText edtCategoryName, edtDescription, editPostion;
    TextView mTitle;
    ProgressDialog loadingBar;
    FirebaseUser fuser;

    //List
    RecyclerView rvCategory;
    List<Category> mCategory;
    CategoryDAO categoryDAO;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Call menuItem
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        //Init layout id
        init(view);
        //Data
        mCategory = categoryDAO.readAllCategoryToRecyclerView(rvCategory);
        fuser = FirebaseAuth.getInstance().getCurrentUser();
        //Add new Category

        return view;
    }

    private void insertCategory() {
        String txt_catName = edtCategoryName.getText().toString().trim();
        String txt_description = edtDescription.getText().toString().trim();
        String txt_position = editPostion.getText().toString().trim();
        if (TextUtils.isEmpty(txt_description) || TextUtils.isEmpty(txt_catName) || TextUtils.isEmpty(txt_position)) {
            Toast.makeText(getContext(), "Bạn chưa nhập đủ dữ liệu", Toast.LENGTH_SHORT).show();
        } else {
            Category category = new Category();
            category.setTitle(txt_catName);
            category.setDescription(txt_description);
            category.setPosition(txt_position);
            categoryDAO.insert(category);
        }
    }

    private void formCategoryDialog(){

        //Dialog
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getLayoutInflater().getContext());

        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.form_category, null);

        edtCategoryName = v.findViewById(R.id.edt_category_name);
        edtDescription = v.findViewById(R.id.edt_description);
        editPostion = v.findViewById(R.id.edt_position);
        mTitle = v.findViewById(R.id.title);
        mTitle.setText("Thêm thể loại");
        builder.setView(v);

        //Action
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                insertCategory();
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();



    }

    private void init(View view) {
        //Toolbar
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Quản lý thể loại");

        rvCategory = view.findViewById(R.id.lvCategory);
        rvCategory.setHasFixedSize(true);
        rvCategory.setLayoutManager(new LinearLayoutManager(getContext()));
        mCategory = new ArrayList<>();
        loadingBar = new ProgressDialog(getContext());
        categoryDAO = new CategoryDAO(getContext());

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_new_category, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_category:
                formCategoryDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
