package com.danghai.dangdhps10201.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.danghai.dangdhps10201.adapter.ViewPagerAdapter;
import com.danghai.dangdhps10201.dao.BookDAO;
import com.danghai.dangdhps10201.dao.CategoryDAO;
import com.danghai.dangdhps10201.model.Book;
import com.danghai.dangdhps10201.model.Category;
import com.danghai.dangdhps10201.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import static android.app.Activity.RESULT_OK;



public class AllFragment extends Fragment {

    EditText edtTitle, edtAuthor, edtPulisher, edtQuantity, edtPrice;
    Spinner spCategory;

    List<Category> mCategory;
    List<Book> mBook;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter pagerAdapter;

    CategoryDAO categoryDAO;
    BookDAO bookDAO;
    //Upload image







    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_all, container, false);

        //Toolbar
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Sách");


        mCategory = new ArrayList<>();
        mBook = new ArrayList<>();
        //Tab
        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);
        pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());


        //data
        categoryDAO = new CategoryDAO(getContext());
        bookDAO = new BookDAO(getContext());

        mCategory = categoryDAO.readAllCategoryToBookFragment(pagerAdapter);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;

    }




    private void insertBook() {
        int pos = spCategory.getSelectedItemPosition();
        String txtCategory = mCategory.get(pos).getId();
        String txtCatTitle = mCategory.get(pos).getTitle();
        String txtBookTitle = edtTitle.getText().toString().trim();
        String txtAuthor = edtAuthor.getText().toString().trim();
        String txtPulisher = edtPulisher.getText().toString().trim();
        String txtQuantity = edtQuantity.getText().toString().trim();
        String txtPrice = edtPrice.getText().toString().trim();


        if (TextUtils.isEmpty(txtBookTitle) || TextUtils.isEmpty(txtPulisher) ||
                TextUtils.isEmpty(txtQuantity) || TextUtils.isEmpty(txtPrice)) {
            Toast.makeText(getContext(), "Bạn chưa nhập đủ fields", Toast.LENGTH_SHORT).show();
        } else {
            Book book = new Book();
            book.setTitle(txtBookTitle);
            book.setCatId(txtCategory);
            book.setCatTitle(txtCatTitle);
            book.setAuthor(txtAuthor);
            book.setPublisher(txtPulisher);
            book.setQuantity(txtQuantity);
            book.setPrice(txtPrice);

            bookDAO.insert(book);
        }

    }

    private void formBookDialog() {
        //Dialog
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getLayoutInflater().getContext());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.form_book, null);
        edtTitle = v.findViewById(R.id.edt_book_title);
        edtAuthor = v.findViewById(R.id.edt_author);
        spCategory = v.findViewById(R.id.sp_category);
        edtPulisher = v.findViewById(R.id.edt_publisher);
        edtQuantity = v.findViewById(R.id.edt_quantity);
        edtPrice = v.findViewById(R.id.edt_price);
        builder.setView(v);

        mCategory = categoryDAO.readAllCategoryToSpinner(spCategory);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                insertBook();

            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_book, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.add_book:
                formBookDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }





}
