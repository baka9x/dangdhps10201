package com.danghai.dangdhps10201.model;

public class Bill {

    private String id;
    private String date;
    private String userId;

    public Bill() {
    }


    public Bill(String id, String date, String userId) {
        this.id = id;
        this.date = date;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
