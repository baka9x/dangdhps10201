package com.danghai.dangdhps10201.model;


public class Cart{
    private String id;
    private String thumbnail;
    private String title;
    private String category;
    private String publisher;
    private String price;
    private String author;
    private String userId;
    private String quantity;


    public Cart() {
    }

    public Cart(String id, String thumbnail, String title, String category, String publisher, String price, String author, String userId, String quantity) {
        this.id = id;
        this.thumbnail = thumbnail;
        this.title = title;
        this.category = category;
        this.publisher = publisher;
        this.price = price;
        this.author = author;
        this.userId = userId;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
