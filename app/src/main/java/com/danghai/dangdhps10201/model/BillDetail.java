package com.danghai.dangdhps10201.model;

public class BillDetail {
    private String id;
    private String bookTitle;
    private String bookPrice;
    private String bookQuantity;
    private String billId;

    public BillDetail() {
    }

    public BillDetail(String bookTitle, String bookPrice, String bookQuantity) {
        this.bookTitle = bookTitle;
        this.bookPrice = bookPrice;
        this.bookQuantity = bookQuantity;
    }

    public BillDetail(String id, String bookTitle, String bookPrice, String bookQuantity, String billId) {
        this.id = id;
        this.bookTitle = bookTitle;
        this.bookPrice = bookPrice;
        this.bookQuantity = bookQuantity;
        this.billId = billId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(String bookPrice) {
        this.bookPrice = bookPrice;
    }

    public String getBookQuantity() {
        return bookQuantity;
    }

    public void setBookQuantity(String bookQuantity) {
        this.bookQuantity = bookQuantity;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }
}
